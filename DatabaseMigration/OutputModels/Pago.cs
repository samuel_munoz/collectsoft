﻿using LiteDB;
using System;

namespace DatabaseMigration.OutputModels
{
    public class Pago
    {
        public ObjectId Id { get; set; }
        public Cliente Cliente { get; set; }
        public decimal Pagado { get; set; }
        public decimal Pendiente { get; set; }
        public DateTime PagadoEn { get; set; }
    }
}
