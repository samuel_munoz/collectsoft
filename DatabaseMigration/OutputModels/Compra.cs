﻿using LiteDB;
using System;

namespace DatabaseMigration.OutputModels
{
    public class Compra
    {
        public ObjectId Id { get; set; }
        public Cliente Cliente { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
        public DateTime CompradoEn { get; set; }
    }
}
