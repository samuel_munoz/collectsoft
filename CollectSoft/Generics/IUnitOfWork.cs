﻿using CollectSoft.Interfaces;
using System;

namespace CollectSoft.Generics
{
    public interface IUnitOfWork : IDisposable
    {
        IClienteRepository Cliente { get; }
        ICompraRepository Compra { get; }
        IPagoRepository Pago { get; }
        int SaveChanges();
    }
}
