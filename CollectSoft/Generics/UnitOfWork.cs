﻿using CollectSoft.Interfaces;
using CollectSoft.Context;
using CollectSoft.Repositories;

namespace CollectSoft.Generics
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CollectContext _context;

        public UnitOfWork(CollectContext context)
        {
            _context = context;
            Cliente = new ClienteRepository(_context);
            Compra = new CompraRepository(_context);
            Pago = new PagoRepository(_context);
        }

        public IClienteRepository Cliente { get; }
        public ICompraRepository Compra { get; }
        public IPagoRepository Pago { get; }

        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
