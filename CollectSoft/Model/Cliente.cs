﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CollectSoft.Model
{
    [Table("clientes")]
    public class Cliente
    {
        [Column("_id")]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public decimal Limite { get; set; }
        public decimal Adeudado { get; set; }
    }
}
