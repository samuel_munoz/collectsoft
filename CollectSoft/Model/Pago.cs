﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CollectSoft.Model
{
    [Table("pagos")]
    public class Pago
    {
        [Column("_id")]
        public int Id { get; set; }
        [Column("ClienteID")]
        public string ClienteId { get; set; }
        public decimal Pagado { get; set; }
        public decimal Pendiente { get; set; }
        [Column("Fecha")]
        public string PagadoEn { get; set; }
    }
}
