﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CollectSoft.Model
{
    [Table("compras")]
    public class Compra
    {
        [Column("_id")]
        public int Id { get; set; }
        [Column("ClienteID")]
        public string Cliente { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
        [Column("Fecha")]
        public string CompradoEn { get; set; }
    }
}
