﻿using CollectSoft.Model;

namespace CollectSoft.ViewModel
{
    public class PagoViewModel
    {
        public Cliente Cliente { get; set; }
        public Pago Pago { get; set; }
    }
}
