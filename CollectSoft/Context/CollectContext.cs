﻿using CollectSoft.Model;
using System.Data.Entity;

namespace CollectSoft.Context
{
    public class CollectContext : DbContext
    {
        public CollectContext() : base("name=collectdb") { }

        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Compra> Compra { get; set; }
        public DbSet<Pago> Pago { get; set; }
    }
}
