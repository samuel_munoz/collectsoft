﻿using CollectSoft.Generics;
using CollectSoft.Model;

namespace CollectSoft.Interfaces
{
    public interface IClienteRepository : IRepository<Cliente>
    {
    }
}
