﻿using CollectSoft.Generics;
using CollectSoft.Model;

namespace CollectSoft.Interfaces
{
    public interface ICompraRepository : IRepository<Compra>
    {
        void Delete(int id);
    }
}
