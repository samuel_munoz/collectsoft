﻿using CollectSoft.Generics;
using CollectSoft.Model;

namespace CollectSoft.Interfaces
{
    public interface IPagoRepository : IRepository<Pago>
    {
        void Delete(int id);
    }
}
