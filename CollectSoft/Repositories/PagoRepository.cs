﻿using CollectSoft.Generics;
using CollectSoft.Interfaces;
using CollectSoft.Model;
using CollectSoft.Context;

namespace CollectSoft.Repositories
{
    public class PagoRepository : Repository<Pago>, IPagoRepository
    {
        public PagoRepository(CollectContext context) : base(context)
        {
        }

        public CollectContext CollectContext => Context as CollectContext;

        public void Delete(int id)
        {
            var data = Context.Set<Pago>().Find(id);
            Context.Set<Pago>().Remove(data);
        }
    }
}
