﻿using CollectSoft.Interfaces;
using CollectSoft.Model;
using CollectSoft.Generics;
using CollectSoft.Context;

namespace CollectSoft.Repositories
{
    public class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public ClienteRepository(CollectContext context) : base(context)
        {
        }

        public CollectContext CollectContext => Context as CollectContext;
    }
}
