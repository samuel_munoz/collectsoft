﻿using CollectSoft.Generics;
using CollectSoft.Interfaces;
using CollectSoft.Model;
using CollectSoft.Context;

namespace CollectSoft.Repositories
{
    public class CompraRepository : Repository<Compra>, ICompraRepository
    {
        public CompraRepository(CollectContext context) : base(context)
        {
        }

        public CollectContext CollectContext => Context as CollectContext;

        public void Delete(int id)
        {
            var data = Context.Set<Compra>().Find(id);
            Context.Set<Compra>().Remove(data);
        }
    }
}
