﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Model;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientSell : Form
    {
        public ClientSell(Cliente obj)
        {
            InitializeComponent();

            clienteBindingSource.DataSource = obj;
            compraBindingSource.DataSource = new Compra();
        }

        private void btnGuardar_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (var unit = new UnitOfWork(new CollectContext()))
                {
                    var compra = compraBindingSource.Current as Compra;
                    var cliente = clienteBindingSource.Current as Cliente;
                    cliente.Adeudado += compra.Cantidad;
                    compra.Cliente = cliente.Id.ToString(); 
                    compra.CompradoEn = dtpFecha.Value.ToShortDateString();
                    unit.Compra.Add(compra);
                    unit.SaveChanges();
                    unit.Cliente.Update(cliente);
                    unit.SaveChanges();
                    MessageBox.Show("Creado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }
            catch
            {
                MessageBox.Show("Error al insertar la venta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCantidad_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(txtCantidad.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCantidad.Text = string.Empty;
                e.Cancel = true;
            }
        }
    }
}
