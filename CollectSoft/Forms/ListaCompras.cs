﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Model;
using System;
using System.Linq;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ListaCompras : Form
    {
        public ListaCompras(Cliente obj)
        {
            InitializeComponent();

            CargarGrid(obj);
        }

        private async void CargarGrid(Cliente obj)
        {
            using (var unit = new UnitOfWork(new CollectContext()))
            {
                var cliente = obj.Id.ToString();
                Text = "Cargando...";
                var result = await unit.Compra.FindAsync(x => x.Cliente == cliente);
                compraBindingSource.DataSource = result.ToList();
                Text = "Listado de compras";
            }
        }

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.CurrentRow == null) return;
            var obj = compraBindingSource.Current as Compra;
            if (MessageBox.Show("¿Desea borrar la compra?", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    using (var unit = new UnitOfWork(new CollectContext()))
                    {
                        var cliente = unit.Cliente.Get(Convert.ToInt32(obj.Cliente));
                        cliente.Adeudado -= obj.Cantidad;
                        unit.Cliente.Update(cliente);
                        unit.SaveChanges();
                        unit.Compra.Delete(obj.Id);
                        unit.SaveChanges();
                        Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Error al borrar la compra", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
