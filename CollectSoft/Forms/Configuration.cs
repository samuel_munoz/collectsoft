﻿using CollectSoft.Properties;
using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace CollectSoft.Forms
{
    public partial class Configuration : Form
    {
        public Configuration()
        {
            InitializeComponent();
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            var ports = SerialPort.GetPortNames();
            foreach (var port in ports)
            {
                comboBox.Items.Add(port);
            }

            comboBox.Text = Settings.Default.Port;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Settings.Default.Port = comboBox.Text;
                Settings.Default.Save();
            }
            catch
            {
                MessageBox.Show("Error al salvar la configuración", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
