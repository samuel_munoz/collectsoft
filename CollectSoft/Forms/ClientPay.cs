﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Helpers;
using CollectSoft.Model;
using CollectSoft.ViewModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientPay : Form
    {
        public ClientPay(Cliente obj)
        {
            InitializeComponent();

            clienteBindingSource.DataSource = obj;
            pagoBindingSource.DataSource = new Pago();
        }

        private void btnPay_Click(object sender, System.EventArgs e)
        {
            Save(false);
        }

        private void Save(bool print = true)
        {
            try
            {
                using (var unit = new UnitOfWork(new CollectContext()))
                {
                    var pago = pagoBindingSource.Current as Pago;
                    var cliente = clienteBindingSource.Current as Cliente;
                    cliente.Adeudado -= pago.Pagado;
                    pago.ClienteId = cliente.Id.ToString();
                    pago.Pendiente = cliente.Adeudado;
                    pago.PagadoEn = dtpFecha.Value.ToShortDateString();
                    unit.Pago.Add(pago);
                    unit.SaveChanges();
                    unit.Cliente.Update(cliente);
                    unit.SaveChanges();
                    var vm = new PagoViewModel { Pago = pago, Cliente = cliente };

                    if (!print)
                    {
                        MessageBox.Show("Abono guardado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        if (!vm.Imprimir())
                        {
                            MessageBox.Show("Error al imprimir", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error al insertar el pago", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPayAndPrint_Click(object sender, System.EventArgs e)
        {
            Save(true);
        }

        private void txtCantidad_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(txtCantidad.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCantidad.Text = string.Empty;
                e.Cancel = true;
            }
        }
    }
}
