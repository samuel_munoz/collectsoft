﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Model;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class CreateClientForm : Form
    {
        bool isEdit = false;

        public CreateClientForm(Cliente obj)
        {
            InitializeComponent();

            if (obj == null)
            {
                clienteBindingSource.DataSource = new Cliente(); 
                isEdit = false;
            }
            else
            {
                clienteBindingSource.DataSource = obj;
                isEdit = true;
            }
        }

        private void btnAgregar_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (var unit = new UnitOfWork(new CollectContext()))
                {
                    if (!isEdit)
                    {
                        unit.Cliente.Add(clienteBindingSource.Current as Cliente);
                        unit.SaveChanges();
                        MessageBox.Show("Creado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    }
                    else
                    {
                        unit.Cliente.Update(clienteBindingSource.Current as Cliente);
                        unit.SaveChanges();
                        MessageBox.Show("Cliente actualizado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    }
                    Close();
                }
            }
            catch
            {
                MessageBox.Show("Error al agregar el cliente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtLimite_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(txtLimite.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtLimite.Text = string.Empty;
                e.Cancel = true;
            }
        }
    }
}
