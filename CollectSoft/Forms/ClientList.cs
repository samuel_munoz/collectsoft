﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Model;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientList : Form
    {
        public ClientList()
        {
            InitializeComponent();
        }

        private async void ClientList_Load(object sender, System.EventArgs e)
        {
            Text = "Listado de clientes - Cargando...";
            await LoadListBox();
            Text = "Listado de clientes";
        }

        private async Task LoadListBox()
        {
            using (var unit = new UnitOfWork(new CollectContext()))
            {
                clienteBindingSource.DataSource = await unit.Cliente.GetAllAsync();
            }
        }

        private async void Search(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                using (var unit = new UnitOfWork(new CollectContext()))
                {
                    #pragma warning disable RCS1155 // Use StringComparison when comparing strings.
                    clienteBindingSource.DataSource = unit.Cliente.Find(x => x.Nombre.ToLower().StartsWith(text.ToLower())).ToList();
                    #pragma warning restore RCS1155 // Use StringComparison when comparing strings.
                } 
            }
            else
            {
                await LoadListBox();
            }
        }

        private void listBox_DoubleClick(object sender, System.EventArgs e)
        {
            var obj = clienteBindingSource.Current as Cliente;
            new ClientDashboard(obj).ShowDialog();
        }

        private void txtNombre_TextChanged(object sender, System.EventArgs e)
        {
            Search(txtNombre.Text);
        }
    }
}
