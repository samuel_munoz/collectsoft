﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Helpers;
using CollectSoft.ViewModel;
using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft.Forms
{
    public partial class PrintAgain : Form
    {
        public PrintAgain()
        {
            InitializeComponent();
        }

        private void textBox_Validating(object sender, CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(textBox.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox.Text = string.Empty;
                e.Cancel = true;
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            using (var unit = new UnitOfWork(new CollectContext()))
            {
                var pago = unit.Pago.Get(Convert.ToInt32(textBox.Text));
                var cliente = unit.Cliente.Get(Convert.ToInt32(pago.ClienteId));
                var vm = new PagoViewModel { Cliente = cliente, Pago = pago };
                if (!vm.Imprimir())
                {
                    MessageBox.Show("Error al imprimir", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Close();
            }
        }
    }
}
