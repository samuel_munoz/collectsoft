﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Model;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientDashboard : Form
    {
        public ClientDashboard(Cliente obj)
        {
            InitializeComponent();

            clienteBindingSource.DataSource = obj;
        }

        private void btnAddVenta_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ClientSell(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void UpdateData(int id)
        {
            using (var unit = new UnitOfWork(new CollectContext()))
            {
                clienteBindingSource.DataSource = unit.Cliente.Get(id);
            }
        }

        private void btnEditarCliente_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new CreateClientForm(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void btnCobrar_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ClientPay(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void btnVentasCliente_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ListaCompras(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void btnPagoCliente_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ListaPagos(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }
    }
}
