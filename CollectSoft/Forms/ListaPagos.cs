﻿using CollectSoft.Context;
using CollectSoft.Generics;
using CollectSoft.Model;
using System;
using System.Linq;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ListaPagos : Form
    {
        public ListaPagos(Cliente obj)
        {
            InitializeComponent();
            CargarGrid(obj);
        }

        private async void CargarGrid(Cliente obj)
        {
            using (var unit = new UnitOfWork(new CollectContext()))
            {
                var cliente = obj.Id.ToString();
                Text = "Cargando...";
                var pago = await unit.Pago.FindAsync(x => x.ClienteId == cliente);
                pagoBindingSource.DataSource = pago.ToList();
                Text = "Listado de pagos";
            }
        }

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.CurrentRow == null) return;
            var obj = pagoBindingSource.Current as Pago;
            if (MessageBox.Show("¿Desea borrar el pago realizado?", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    using (var unit = new UnitOfWork(new CollectContext()))
                    {
                        var cliente = unit.Cliente.Get(Convert.ToInt32(obj.ClienteId));
                        cliente.Adeudado += obj.Pagado;
                        unit.Cliente.Update(cliente);
                        unit.SaveChanges();
                        unit.Pago.Delete(obj.Id);
                        unit.SaveChanges();
                        Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Error al borrar el pago", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
